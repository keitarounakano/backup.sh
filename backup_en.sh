#!/bin/bash

BACKUP_DIR=("/var/www")

BACKUP_ROOT='/backup'

DB_BACKUP_ENABLE=1

DB_USER="root"

DB_PASS="n3o2C8FHg1UM"

ALL_DATABASES_ENABLE=1

DB_NAMES=()

BACKUP_GENERATIONS=3

BACKUP_TRANSFER_ENABLE=1

BACKUP_SERVER_CONNECT="scp -rp -P 10022 backupuser@bitonesystem-backup.z-hosts.com"

BACKUP_SERVER_DIR="/home/backupuser/backup"

LOG_PATH="/var/log/backup/backup.log"

mk_tmp_dir() {
	
	DATE=`date +%Y%m%d`
	TMP_DB_FILE=db_all_${DATE}
	TMP_DIR_NAME=backup_${DATE}
	TMP_DIR=/tmp/${TMP_DIR_NAME}

	if [ -e ${TMP_DIR} ]; then
		ID=1
		TMP_DIR_NAME=backup_${DATE}_${ID}
		TMP_DIR=/tmp/${TMP_DIR_NAME}
		
		while [ -e ${TMP_DIR} ]
		do
			ID=`expr ${ID} + 1`
			TMP_DIR_NAME=backup_${DATE}_${ID}
			TMP_DIR=/tmp/${TMP_DIR_NAME}
		done
	fi
	
	mkdir ${TMP_DIR}
	
	echo_log "create temp dir ${TMP_DIR}"
	
}

data_backup() {
	
	for dir in ${BACKUP_DIR[@]}
	do
		echo_log "copy start:"${dir}
		cp -ar ${dir} ${TMP_DIR}/
		echo_log "copy end:"${dir}
	done

}

db_dump() {

	if [ ${ALL_DATABASES_ENABLE} -eq 1 ]; then
		echo_log "all dump start"
		mysqldump -u ${DB_USER} -p${DB_PASS} --all-databases > ${TMP_DIR}/${TMP_DB_FILE}.sql
		echo_log "all dump end"
	elif [ ${ALL_DATABASES_ENABLE} -eq 0 ]; then
		mkdir ${TMP_DIR}/databases
		for db in ${DB_NAMES[@]}
		do
			echo_log "dump start:"${db}
			mysqldump -u ${DB_USER} -p${DB_PASS} ${db} > ${TMP_DIR}/databases/${db}_${DATE}.sql
			echo_log "dump end:"${db}
		done
	fi
	
}

delete_old_backups() {
	
	NUM=`find ${BACKUP_ROOT} -type f -name "backup_*.tar.gz" | wc -l`
	while [ ${NUM} -gt ${BACKUP_GENERATIONS} ]
	do
		DEL_FILE=`ls -1t ${BACKUP_ROOT}/backup_*.tar.gz | tail -1`
		echo_log "delete old backup file:"${DEL_FILE}
		rm -f ${DEL_FILE}
		NUM=`find ${BACKUP_ROOT} -type f -name "backup_*.tar.gz" | wc -l`
	done
	
}

archive() {

	cd /tmp
	echo_log "create archive file:"${TMP_DIR_NAME}
	tar -zcf ${TMP_DIR_NAME}.tar.gz ${TMP_DIR_NAME}
	mv ${TMP_DIR_NAME}.tar.gz ${BACKUP_ROOT}
	echo_log "end archive file:"${TMP_DIR_NAME}
	rm -rf ${TMP_DIR}
	echo_log "delete tmp dir:"${TMP_DIR}

}

backup_transfer() {
	
	check=`echo ${BACKUP_SERVER_CONNECT} | egrep "(scp|rsync).*[a-z0-9]+@((.*(\.com|\.net|\.jp)$)|[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4})" | wc -l`
	
	if [ ${check} -ne 1 ]; then
		echo_log "did not connect to backup server."
		echo_log "scp or rsync only. and hostname is .com,.net,.jp or ip address."
		return
	fi
	
	server=(`echo ${BACKUP_SERVER_CONNECT} | egrep -o "[a-z0-9]+@.*" | tr -s '@' ' '`)
	user=${server[0]}
	host=${server[1]}
	
	array=(`echo ${BACKUP_SERVER_CONNECT}`)
	lastindex=`expr ${#array[@]} - 1`
	unset array[${lastindex}]
	array=${array[@]}
	
	command="${array} ${BACKUP_ROOT}/${TMP_DIR_NAME}.tar.gz ${user}@${host}:${BACKUP_SERVER_DIR}"
	
	echo_log "transfer start backup file:"${TMP_DIR_NAME}.tar.gz
	eval ${command}
	echo_log "transfer end backup file:"${TMP_DIR_NAME}.tar.gz
}

echo_log() {
	
	if [ -e ${LOG_PATH} ]; then
		echo `date +%Y%m%d%T` >> ${LOG_PATH} $1
	else
		echo "log file not found:"${LOG_PATH}
	fi
	
}

execute() {

	echo_log "backup start"
	
	mk_tmp_dir
	
	data_backup

	if [ ${DB_BACKUP_ENABLE} -eq 1 ]; then
		db_dump
	fi

	archive

	if [ ${BACKUP_TRANSFER_ENABLE} -eq 1 ]; then
		backup_transfer
	fi

	delete_old_backups

	echo_log "backup end"

}

execute