#!/bin/bash

#############################################################
# backup.sh
# 2015/07/22
# データベースはMySQLのみ対応
# バックアップ先サーバのローテーションはしない
# バックアップ先のディレクトリはバックアップファイル以外入れないでください
# LOG_PATHには予めログファイルを作成してください
# バックアップ先サーバへの転送は最新バックアップファイルのみ
#
# 実装予定
# スクリプトチェック
# エラーキャッチ
#############################################################

# バックアップ元のディレクトリ
# 配列指定(スペース区切り)
# 例：BACKUP_DIR=("/var/www" "/home")
BACKUP_DIR=("/var/www")

# バックアップ先のディレクトリ
# フルパスで記載
# 末尾に「/」は不要です
BACKUP_ROOT='/backup'

# DBをバックアップするか
# しない：0, する：1
DB_BACKUP_ENABLE=1

# DBバックアップ時のログインユーザー
# alldatabasesの場合はrootなどの特権ユーザー
DB_USER="root"

# DBユーザーのパスワード
DB_PASS="n3o2C8FHg1UM"

# alldatabasesでバックアップするか
# しない：0, する：1
ALL_DATABASES_ENABLE=1

# DB名
# DB_BACKUP_ENABLE=1、かつALL_DATABASES_ENABLEが0の時のみ有効
# 配列指定(スペース区切り)
# 例：DB_NAMES=("db1" "db2")
DB_NAMES=()

# 世代数
BACKUP_GENERATIONS=3

# バックアップサーバへ転送するか
# しない：0, する：1
BACKUP_TRANSFER_ENABLE=1

# バックアップサーバへの接続コマンド
# 例1："scp -rpv -P 10022 root@example.z-hosts.com"
# 例2："rsync -arv -e 'ssh -p 10022' root@example.z-hosts.com"
# ホスト名の後の「：」は不要です
#BACKUP_SERVER_CONNECT="scp -rp -P 10022 root@example.z-hosts.com"
BACKUP_SERVER_CONNECT="scp -rp -P 10022 backupuser@bitonesystem-backup.z-hosts.com"

# バックアップサーバの保存先ディレクトリ
# /から始まるフルパス形式
BACKUP_SERVER_DIR="/home/backupuser/backup"

# ログファイル
# /から始まるフルパス形式
LOG_PATH="/var/log/backup/backup.log"

# 以下編集不要です
###############################################
#

mk_tmp_dir() {
	
	DATE=`date +%Y%m%d`
	TMP_DB_FILE=db_all_${DATE}
	TMP_DIR_NAME=backup_${DATE}
	TMP_DIR=/tmp/${TMP_DIR_NAME}

	if [ -e ${TMP_DIR} ]; then
		ID=1
		TMP_DIR_NAME=backup_${DATE}_${ID}
		TMP_DIR=/tmp/${TMP_DIR_NAME}
		
		while [ -e ${TMP_DIR} ]
		do
			ID=`expr ${ID} + 1`
			TMP_DIR_NAME=backup_${DATE}_${ID}
			TMP_DIR=/tmp/${TMP_DIR_NAME}
		done
	fi
	
	mkdir ${TMP_DIR}
	
	echo_log "一時ディレクトリ ${TMP_DIR} の作成"
	
}

data_backup() {
	
	for dir in ${BACKUP_DIR[@]}
	do
		echo_log ${dir}"のコピー開始"
		cp -ar ${dir} ${TMP_DIR}/
		echo_log ${dir}"のコピー完了"
	done

}

db_dump() {

	if [ ${ALL_DATABASES_ENABLE} -eq 1 ]; then
		echo_log "データベースのdump開始"
		mysqldump -u ${DB_USER} -p${DB_PASS} --all-databases > ${TMP_DIR}/${TMP_DB_FILE}.sql
		echo_log "データベースのdump完了"
	elif [ ${ALL_DATABASES_ENABLE} -eq 0 ]; then
		mkdir ${TMP_DIR}/databases
		for db in ${DB_NAMES[@]}
		do
			echo_log "データベース："${db}" のdump開始"
			mysqldump -u ${DB_USER} -p${DB_PASS} ${db} > ${TMP_DIR}/databases/${db}_${DATE}.sql
			echo_log "データベース："${db}" のdump完了"
		done
	fi
	
}

delete_old_backups() {
	
	NUM=`find ${BACKUP_ROOT} -type f -name "backup_*.tar.gz" | wc -l`
	while [ ${NUM} -gt ${BACKUP_GENERATIONS} ]
	do
		DEL_FILE=`ls -1t ${BACKUP_ROOT}/backup_*.tar.gz | tail -1`
		echo_log "古いバックアップファイル："${DEL_FILE}の削除
		rm -f ${DEL_FILE}
		NUM=`find ${BACKUP_ROOT} -type f -name "backup_*.tar.gz" | wc -l`
	done
	
}

archive() {

	cd /tmp
	echo_log ${TMP_DIR_NAME}"のアーカイブ作成"
	tar -zcf ${TMP_DIR_NAME}.tar.gz ${TMP_DIR_NAME}
	mv ${TMP_DIR_NAME}.tar.gz ${BACKUP_ROOT}
	echo_log ${TMP_DIR_NAME}"のアーカイブ作成完了"
	rm -rf ${TMP_DIR}
	echo_log "一時ディレクトリ："${TMP_DIR}"の削除"

}

backup_transfer() {
	
	check=`echo ${BACKUP_SERVER_CONNECT} | egrep "(scp|rsync).*[a-z0-9]+@((.*(\.com|\.net|\.jp)$)|[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4})" | wc -l`
	
	if [ ${check} -ne 1 ]; then
		echo_log "バックアップサーバへの接続コマンドが正しくありません。"
		echo_log "scpかrsyncコマンドで、「ユーザー名@ホスト名(.com、.net、jp、またはIPアドレス)」である必要があります。"
		echo_log "転送を中断しました"
		return
	fi
	
	server=(`echo ${BACKUP_SERVER_CONNECT} | egrep -o "[a-z0-9]+@.*" | tr -s '@' ' '`)
	user=${server[0]}
	host=${server[1]}
	
	array=(`echo ${BACKUP_SERVER_CONNECT}`)
	lastindex=`expr ${#array[@]} - 1`
	unset array[${lastindex}]
	array=${array[@]}
	
	command="${array} ${BACKUP_ROOT}/${TMP_DIR_NAME}.tar.gz ${user}@${host}:${BACKUP_SERVER_DIR}"
	
	echo_log "バックアップファイル："${TMP_DIR_NAME}.tar.gz"の転送開始"
	eval ${command}
	echo_log "バックアップファイル："${TMP_DIR_NAME}.tar.gz"の転送完了"
}

echo_log() {
	
	if [ -e ${LOG_PATH} ]; then
		echo `date +%Y%m%d%T` >> ${LOG_PATH} $1
	else
		echo "ログファイル："${LOG_PATH}"が見つかりません"
	fi
	
}

execute() {

	echo_log "バックアップ開始"
	
	mk_tmp_dir
	
	data_backup

	if [ ${DB_BACKUP_ENABLE} -eq 1 ]; then
		db_dump
	fi

	archive

	if [ ${BACKUP_TRANSFER_ENABLE} -eq 1 ]; then
		backup_transfer
	fi

	delete_old_backups

	echo_log "バックアップ完了"

}

execute